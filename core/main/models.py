from tabnanny import verbose
from django.db import models


class FileCSV(models.Model):
    title = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)
    document = models.FileField(upload_to="media/%Y/%m/%d/")

    class Meta:
        verbose_name = "Файл"
        verbose_name_plural = "Файлы"
    
    def __str__(self):
        return self.title