from django.urls import include, path
from main.views import get, addfile, collections, addfile2
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('people/', get, name="home"),
    path('addfile/', addfile, name="addfile"),
    path('collections/', collections, name = 'collections'),
    path('addfile2/', addfile2, name="addfile2"),
    
]
urlpatterns += static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)