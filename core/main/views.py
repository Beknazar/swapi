import csv
import json
from datetime import datetime
from math import ceil
import requests
from django.core.files.base import ContentFile
from django.shortcuts import redirect, render
from main.models import FileCSV


count_objects_all = int(
                        json.loads(
                        (requests.get('https://swapi.dev/api/people/').text))['count']) #count_objects_all --- количества всех обьектов(людей) в запросе
count_objects_in_page = len(
                        json.loads(
                        (requests.get('https://swapi.dev/api/people/').text))['results']) #count_objects_in_page --- количества всех обьектов(людей) в одной странице

count_page = ceil(count_objects_all // count_objects_in_page) #count_page --- количество всех страниц в запросе

blackbox = ["films", 
            "species", 
            "vehicles", 
            "starships", 
            "url", 
            "edited"] #минуемые значение цикла в Templates

get_request = ["homeworld"] #требуют запрос с сервера(содержат url) "homeworld":"https://swapi.dev/api/planets/21/"
get_valid_date = ["created"] #требуют редактирования "created":"2014-12-10T16:26:56.138000Z"
get_clean_object = {} #пустой словарь для очистки данных
page = 1 #дефолтная страница


def get(request):
    """
    Отпрвка GET запросов

    """
    get_real_planet_name = get_clean_object.copy() #словарь для наименования планет 
    valid_date = get_clean_object.copy() #cловарь для редактируемой даты
    page = request.GET.get('page', 1)
    url = f'https://swapi.dev/api/people/?page={page}'
    response = requests.get(url)
    data_response = json.loads(response.text) #преобразования str в dict
    
    for i in data_response["results"]:
        get_real_planet_name[i["name"]] = i["homeworld"]  #get_real_planet_name = {"Anakin Skywalker": "https://swapi.dev/api/planets/1/"}
    
    for key, value in get_real_planet_name.items():
        planet = requests.get(value)
        data_planet = json.loads(planet.text)
        get_real_planet_name[key] = data_planet["name"]  #get_real_planet_name = {"Anakin Skywalker": "Tatooine"}
    
    for i in data_response["results"]:
        valid_date[i["name"]] = i["created"] #valid_date = {"Luke Skywalker": "2014-12-09T13:50:51.644000Z"}
    
    for key, value in valid_date.items():
        valid_date[key] = '-'.join(value.split('T')[:-1]) #valid_date = {"Luke Skywalker": "2014-12-09"}
    
    def validation_next_page(data): # функция для получения следующей страницы ?page=2
        if not data['next']:
            return ""
        return data['next'][29:]

    return render(request, 
                    'main/people.html', {
                    "people":           data_response,
                    "blackbox":         blackbox,
                    "planet":           get_real_planet_name,
                    "planet_not_valid": get_request,
                    "next_page":        validation_next_page(data_response),
                    "get_date":         get_valid_date,
                    "valid_date":       valid_date,
                    "validated":        validation_page(data_response),
                    "real_page":        page})


def validation_page(data): #функция для валидного отображения кнопки (Load more) в templates
        if not data['next']:
            return False
        return True



def addfile(request):
    """
    Загрузка файлов csv
    """
    get_real_planet_name = get_clean_object.copy() #словарь для наименования планет 
    valid_date = get_clean_object.copy() #cловарь для редактируемой даты
    if request.method == 'POST':
        page = request.POST['pk']
        url = f'https://swapi.dev/api/people/?page={page}'
        response = requests.get(url)
        data_response = json.loads(response.text)#преобразования str в dict
        
        for i in data_response["results"]:
            get_real_planet_name[i["name"]] = i["homeworld"] #get_real_planet_name = {"Anakin Skywalker": "https://swapi.dev/api/planets/1/"}
            
        for key, value in get_real_planet_name.items():
            planet = requests.get(value)
            data_planet = json.loads(planet.text)
            get_real_planet_name[key] = data_planet["name"] #get_real_planet_name = {"Anakin Skywalker": "Tatooine"}
        
        for i in data_response["results"]:
            valid_date[i["name"]] = i["created"] #valid_date = {"Luke Skywalker": "2014-12-09T13:50:51.644000Z"}
        
        for key, value in valid_date.items():
            valid_date[key] = '-'.join(value.split('T')[:-1]) #valid_date = {"Luke Skywalker": "2014-12-09"}
        
        file = str(datetime.now()).replace(":", "").replace("-", "").replace(" ", "").replace(".", "") #название файла
        file_txt = "" #переменная для записи данных для обработки
    
        for i in data_response['results'][0]:
            if i not in blackbox:
                file_txt += (f'{i}\t')
        file_txt += "\n" 

        for i in data_response['results']:
            for key, value in i.items():
                if key not in blackbox: #условие для минования
                    if key not in get_request:#get_request --- минуем ссылку планеты
                        if key not in get_valid_date:#минуем дату
                            file_txt += f'{value}\t'
                        else:
                            for key, value in valid_date.items():
                                if key == i['name']:
                                    file_txt += f'{value}\t'# подставляем валидную дату
                    else:
                        for key, value in get_real_planet_name.items():
                            if key == i['name']:
                                file_txt += f'{value}\t' #подставляем название планеты
            file_txt += "\n"
            
        content_file = ContentFile(file_txt, name=f'{file}.csv')#создаем файл и заполняем его данными
        instance = FileCSV(document=content_file, title=file)#создаем обьект FileCSV
        instance.save()
        return redirect('home')#возвращаемся на главную страницу
    else:
        file = FileCSV()
    return render(request, 'main/index.html')


def addfile2(request):
    page = request.POST['pk']
    url = f"http://127.0.0.1:8000/swapi.co/api/people/?page={page}"
    response = requests.get(url)
    with open("file.csv", 'w') as file:
        file.write(response.text)
    return redirect("home")
    



def collections(request):
    """
    Отображение файлов и его содержимое
    """
    queryset = FileCSV.objects.all()
    pk = request.GET.get('pk', None)
    if not pk:
        return render(request, 'main/collections.html', {"files": queryset})
    file_name = get_clean_object.copy()
    rows_data = []
    rows = []
    file = FileCSV.objects.get(pk=pk)
    file_csv = str(file.document)
    file_name = file.title
    with open(file_csv, 'r') as file:
        csvreader = csv.reader(file)
        header = next(csvreader)
        for row in csvreader:
            rows.append(row)

    for i in rows:
        if len(i) > 1:
            rows_data.append(" ".join(i).split("\t"))
        else:
            rows_data.append(i[0].split("\t"))

    return render(request, 
                    'main/fileview.html', {
                    "header":   header[0].split("\t"), 
                    "rows":     rows_data,
                    "files":    queryset,
                    "file_name":file_name
                    })